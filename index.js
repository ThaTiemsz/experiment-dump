const fs = require("fs");
const readline = require("readline");
let user = {}
let guild = {}

let state = {
  tns: 0,
  activity: 0
}

function process(i){
  if(i.event_type == "experiment_user_triggered"){
    if(!user[i.name]){
      user[i.name] = {
        buckets: [],
        revisions: []
      }
    }
    if(!user[i.name].buckets.includes(i.bucket)){
      user[i.name].buckets.push(i.bucket)
    }
    if(!user[i.name].revisions.includes(i.revision)){
      user[i.name].revisions.push(i.revision)
    }
  }
  if(i.event_type == "experiment_guild_triggered"){
    if(!guild[i.name]){
      guild[i.name] = {
        buckets: [],
        revisions: []
      }
    }
    if(!guild[i.name].buckets.includes(i.bucket)){
      guild[i.name].buckets.push(i.bucket)
    }
    if(!guild[i.name].revisions.includes(i.revision)){
      guild[i.name].revisions.push(i.revision)
    }
  }
}

function finish(){
  if(state.tns != 0 && state.activity != 0){
    fs.writeFileSync('./output-guild.json', JSON.stringify(guild, null, 2))
    fs.writeFileSync('./output-user.json', JSON.stringify(user, null, 2))
  }
}

const readInterfaceTns = readline.createInterface({
  input: fs.createReadStream("./tns.json"),
  console: false,
})

const readInterfaceActivity = readline.createInterface({
  input: fs.createReadStream("./activity.json"),
  console: false,
})

readInterfaceTns.on('line', (input) => {
  process(JSON.parse(input));
})

readInterfaceActivity.on('line', (input) => {
  process(JSON.parse(input));
})
  
readInterfaceTns.on('close', () => {
  state.tns = 1
  finish()
})

readInterfaceActivity.on('close', () => {
  state.activity = 1
  finish()
})
