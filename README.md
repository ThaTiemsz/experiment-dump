# experiment-dump

extracts experiments you've been in from your discord data export

## Usage

Open your package.zip and navigate to the `activity` folder

![Root](https://cdn.discordapp.com/attachments/415951527258095616/858283220168998932/unknown.png)

Nagivate to the `analytics` and `tns` folders

![activity](https://cdn.discordapp.com/attachments/415951527258095616/858283362736799784/unknown.png)

Copy the events JSON file into a directory with the `index.js` script

![events](https://cdn.discordapp.com/attachments/415951527258095616/858283461416845312/unknown.png)

Rename the files to `activity.json` and `tns.json`, which is which does not actually matter

![renamed](https://cdn.discordapp.com/attachments/415951527258095616/858285538959818762/unknown.png)

Run `index.js`. It might take a moment to process all events. Once finished, you should find two new `output-guild.json` and `output-user.json` files, listing the guild and user experiments you've been apart of.

![output](https://cdn.discordapp.com/attachments/415951527258095616/858286019828645888/unknown.png)
